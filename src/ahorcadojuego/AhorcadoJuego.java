/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ahorcadojuego;

import javax.swing.JFrame;

public class AhorcadoJuego extends JFrame{

    public static void main(String[] args) {
        try{
            javax.swing.UIManager.setLookAndFeel(
            javax.swing.UIManager.getSystemLookAndFeelClassName());
        }catch ( Exception e ) { 
        } 

        Juego j = new Juego();
        
        j.addKeyListener(j);
        j.setFocusable(true);
        
        j.setTitle("Colgado");
        j.setResizable(false);
        j.setVisible(true);
        j.setLocationRelativeTo(null);
    }
    
}
